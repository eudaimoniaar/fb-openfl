#ifndef STATIC_LINK
#define IMPLEMENT_API
#endif

#if defined(HX_WINDOWS) || defined(HX_MACOS) || defined(HX_LINUX)
#define NEKO_COMPATIBLE
#endif


#include <hx/CFFI.h>
#include "Utils.h"


using namespace fb_haxe_android;

AutoGCRoot* eventHandler = 0;

static value fb_haxe_android_init (value target) {
	printf("init fb for ios\n");
	eventHandler = new AutoGCRoot(target);
	return alloc_null();
	
}
DEFINE_PRIM (fb_haxe_android_init, 1);

static value fb_haxe_android_close () {
	printf("close session\n");
	
	closeSession();

	return alloc_null();
	
}
DEFINE_PRIM (fb_haxe_android_close, 0);

static value fb_haxe_android_get_session_state () {
	printf("is session open\n");
	return alloc_string(getSessionState());
	
}
DEFINE_PRIM (fb_haxe_android_get_session_state, 0);

static value fb_haxe_android_login (value permissions) {
	printf("login fb for ios\n");
	login(val_string(permissions));

	return alloc_null();	
}
DEFINE_PRIM (fb_haxe_android_login, 1);

static value fb_haxe_android_get_request (value path, value parameters, value callback) {
	printf("request fb for ios\n");
	AutoGCRoot* callbackHandler = new AutoGCRoot(callback);

	getRequest(val_string(path), val_string(parameters), callbackHandler);

	return alloc_null();
	
}
DEFINE_PRIM (fb_haxe_android_get_request, 3);

static value fb_haxe_android_share (value link) {
	printf("share fb for ios\n");
	share(val_string(link));

	return alloc_null();	
}
DEFINE_PRIM (fb_haxe_android_share, 1);

static value fb_haxe_android_get_access_token () {
	printf("get fb token for ios\n");
	return alloc_string(getAccessToken());
}
DEFINE_PRIM (fb_haxe_android_get_access_token, 0);

static value fb_haxe_android_set_access_token (value accessToken, value permissions, value appID, value userID, value expires/*, value lastRefresh*/) {
	printf("set fb token for ios\n");
	setAccessToken(val_string(accessToken), val_string(permissions), val_string(appID), val_string(userID), val_string(expires), NULL);

	return alloc_null();
}
DEFINE_PRIM (fb_haxe_android_set_access_token, 5);

static value fb_haxe_android_send_request_dialog (value message, value to) {
	printf("send message dialog fb token for ios\n");
	sendRequestDialog(val_string(message), val_string(to));

	return alloc_null();
}
DEFINE_PRIM (fb_haxe_android_send_request_dialog, 2);

extern "C" void fb_haxe_android_main () {
	
	val_int(0); // Fix Neko init
	
}
DEFINE_ENTRY_POINT (fb_haxe_android_main);



extern "C" int fb_haxe_android_register_prims () { return 0; }

extern "C" void sendSessionStateChangedEvent(const char* state)
{
	val_call1(eventHandler->get(), alloc_string(state));
}