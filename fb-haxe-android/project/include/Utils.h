#ifndef FB_HAXE_ANDROID_H
#define FB_HAXE_ANDROID_H

#include <hx/CFFI.h>

namespace fb_haxe_android {

	extern "C"
    {
		const char* getSessionState();
		void login(const char* permissions);
		void getRequest(const char* path, const char* parameters, AutoGCRoot* callback);
		void share(const char* link);
		void closeSession();
		void setAccessToken(const char* accessToken, const char* permissions, const char* appID, const char* userID, const char* expires, const char* lastRefresh);
		const char* getAccessToken();
		void sendRequestDialog(const char* message, const char* to);
	}
}


#endif