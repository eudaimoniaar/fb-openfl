#import <CoreFoundation/CoreFoundation.h>
#import <UIKit/UIKit.h>
#import <objc/runtime.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

#include "Utils.h"
#include <hx/CFFI.h>

extern "C" void sendSessionStateChangedEvent(const char* state);

@interface FBHelper: NSObject

+(void) load;

@end

@interface NMEAppDelegate : NSObject <UIApplicationDelegate>
@end
@interface NMEAppDelegate (FBHelper)
@end

@implementation NMEAppDelegate (FBHelper)
	- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
		NSLog(@"openURL");
		return [[FBSDKApplicationDelegate sharedInstance] application:application
	                                                        openURL:url
	                                              sourceApplication:sourceApplication
	                                                     annotation:annotation];
	}
@end

	
@implementation FBHelper

+(void) load {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunchingNotification:)
               name:@"UIApplicationDidFinishLaunchingNotification" object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActiveNotification:)
               name:@"UIApplicationDidBecomeActiveNotification" object:nil];
    
}

// This code will be called immediately after application:didFinishLaunchingWithOptions:
+(void) applicationDidFinishLaunchingNotification:(NSNotification *)notification {
    NSDictionary *launchOptions = [notification userInfo] ;

    [[FBSDKApplicationDelegate sharedInstance] application:[UIApplication sharedApplication]
                                  didFinishLaunchingWithOptions:launchOptions];
}

+(void) applicationDidBecomeActiveNotification:(NSNotification *)notification {
	[FBSDKAppEvents activateApp];
}

- (void)dealloc {
	[super dealloc];
}

@end

extern "C"
{
	const char* getSessionState() {
		if ([FBSDKAccessToken currentAccessToken]) {
   			return "OPENED";
  		}
		return "NULL_SESSION";
	}

	void login(const char* permissions) {
		FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];

		NSArray* permissionsArray = [[NSString stringWithUTF8String:permissions] componentsSeparatedByString:@","];

		[login logInWithReadPermissions:permissionsArray handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
		  if (error) {
		    // Process error
		    NSLog(@"Process error");
		    sendSessionStateChangedEvent("CLOSE");
		  } else if (result.isCancelled) {
		    // Handle cancellations
		    NSLog(@"Handle cancellations");
		    sendSessionStateChangedEvent("CLOSE");
		  } else {
		    // If you ask for multiple permissions at once, you
		    // should check if specific permissions missing
		    //if ([result.grantedPermissions containsObject:@"email"]) {
		      // Do work
		    //}
		    sendSessionStateChangedEvent("OPENED");
		    NSLog(@"Log in succesfully!!!");
		  }
		}];
	}

	void closeSession() {
		FBSDKLoginManager *loginManager = [[[FBSDKLoginManager alloc] init] autorelease];

		[loginManager logOut];
	}

	void getRequest(const char* path, const char* parameters, AutoGCRoot* callback) {
		NSLog(@"going to do request with path: %@", [NSString stringWithUTF8String:path]);
		NSString* strParameters = [NSString stringWithUTF8String: parameters];
		NSMutableDictionary* _parameters = nil;
		if(parameters && ![strParameters isEqualToString: @""]){
			_parameters = [[[NSMutableDictionary alloc] init] autorelease];
			NSArray *urlComponents = [strParameters componentsSeparatedByString:@"&"];
			for (NSString *keyValuePair in urlComponents)
			{
			    NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
			    NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
			    NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];

			    [_parameters setObject:value forKey:key];
			}
		}
		[[[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithUTF8String:path] parameters: _parameters]
	    	startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
	    		NSLog(@"completion handler, result: %@, error: %@", result, error.localizedDescription);
		      if (!error) {
		         NSLog(@"fb response:%@", result);
		         
		         NSError *error;
			     NSData *jsonData = [NSJSONSerialization dataWithJSONObject:result
			                                                   options:(NSJSONWritingOptions)NSJSONWritingPrettyPrinted
			                                                     error:&error];

			    NSString* jsonResult = nil;
				if (! jsonData) {
					NSLog(@"NSJSONSerialization: error: %@", error.localizedDescription);
					jsonResult = @"{}";
				} else {
					jsonResult = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
				}

				val_call1(callback->get(), alloc_string([jsonResult UTF8String]));
		      }
	  	}];
	}

	void share(const char* link) {
		NSString* linkURL = [NSString stringWithUTF8String:link];
		FBSDKAppInviteContent *content = [[FBSDKAppInviteContent alloc] initWithAppLinkURL:[NSURL URLWithString:linkURL]]; 

		[FBSDKAppInviteDialog showWithContent:content delegate:nil];
	}

	void setAccessToken(const char* accessToken, const char* permissions, const char* appID, const char* userID, const char* expires, const char* lastRefresh) {

		NSString* tokenString = [NSString stringWithUTF8String:accessToken];
		NSArray* permissionsArray = [[NSString stringWithUTF8String:permissions] componentsSeparatedByString:@","];
		NSString* _appID = [NSString stringWithUTF8String:appID];
		NSString* _userID = [NSString stringWithUTF8String:userID];
		NSDate* expirationDate = [NSDate dateWithTimeIntervalSince1970:[[NSString stringWithUTF8String:expires] doubleValue]];
		NSDate* refreshDate = nil; //[NSDate dateWithTimeIntervalSince1970:[[NSString stringWithUTF8String:lastRefresh] doubleValue]];


		FBSDKAccessToken* token = [[FBSDKAccessToken alloc] 
												initWithTokenString:tokenString
												permissions:permissionsArray
												declinedPermissions:nil
												appID:_appID
												userID:_userID
												expirationDate:expirationDate
												refreshDate:refreshDate];

		[FBSDKAccessToken setCurrentAccessToken:token];
	}
	
	const char* getAccessToken() {
		FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];

		NSString* permissions = [[[accessToken permissions] allObjects] componentsJoinedByString:@","];;

		NSMutableDictionary* accessTokenComponents = [NSMutableDictionary dictionary];
		[accessTokenComponents setObject:[accessToken tokenString] forKey:@"token"];
		[accessTokenComponents setObject:permissions forKey:@"permissions"];
		[accessTokenComponents setObject:[accessToken appID] forKey:@"appID"];
		[accessTokenComponents setObject:[accessToken userID] forKey:@"userID"];
		[accessTokenComponents setObject:[NSString stringWithFormat:@"%f",[[accessToken expirationDate] timeIntervalSince1970]] forKey:@"expires"];
		[accessTokenComponents setObject:[NSString stringWithFormat:@"%f",[[accessToken refreshDate] timeIntervalSince1970]] forKey:@"lastRefresh"];

		//Convert to json string
		NSError *error;
		NSData *jsonData = [NSJSONSerialization dataWithJSONObject:accessTokenComponents
			                                                   options:(NSJSONWritingOptions)NSJSONWritingPrettyPrinted
			                                                     error:&error];
		NSString* jsonResult = nil;
		if (! jsonData) {
			NSLog(@"NSJSONSerialization: error: %@", error.localizedDescription);
			jsonResult = @"{}";
		} else {
			jsonResult = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
		}

		return [jsonResult UTF8String];
	}

	void sendRequestDialog(const char* message, const char* to) {
		FBSDKGameRequestContent *content = [[FBSDKGameRequestContent alloc]init];
		content.message = [NSString stringWithUTF8String:message];
		content.to = [NSArray arrayWithObject:[NSString stringWithUTF8String:to]];

		//FBSDKGameRequestActionTypeTurn 
		[FBSDKGameRequestDialog showWithContent: content
								delegate: nil];

	}
}