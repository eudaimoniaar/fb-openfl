package org.haxe.extension;


import android.app.Activity;
import android.content.res.AssetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.util.Log;
import android.widget.Toast;
import android.view.Window;
import android.view.WindowManager;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.*;
import java.security.MessageDigest;
import android.util.Base64;
import android.content.pm.Signature;
import java.security.NoSuchAlgorithmException;
import android.net.Uri;

import java.util.Set;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Arrays;
import java.util.Date;

import com.facebook.*;
import com.facebook.model.*;
import com.facebook.widget.*;

import org.json.JSONObject;

import org.haxe.lime.HaxeObject;

import android.text.TextUtils;

/* 
	You can use the Android Extension class in order to hook
	into the Android activity lifecycle. This is not required
	for standard Java code, this is designed for when you need
	deeper integration.
	
	You can access additional references from the Extension class,
	depending on your needs:
	
	- Extension.assetManager (android.content.res.AssetManager)
	- Extension.callbackHandler (android.os.Handler)
	- Extension.mainActivity (android.app.Activity)
	- Extension.mainContext (android.content.Context)
	- Extension.mainView (android.view.View)
	
	You can also make references to static or instance methods
	and properties on Java classes. These classes can be included 
	as single files using <java path="to/File.java" /> within your
	project, or use the full Android Library Project format (such
	as this example) in order to include your own AndroidManifest
	data, additional dependencies, etc.
	
	These are also optional, though this example shows a static
	function for performing a single task, like returning a value
	back to Haxe from Java.
*/
public class Fb_haxe_android extends Extension {

	private static UiLifecycleHelper fbUiLifecycleHelper;

	public static HaxeObject sessionStatusListener;

    public static UiLifecycleHelper getFbUiLifecycleHelper() {
        return Fb_haxe_android.fbUiLifecycleHelper;
    }

    public static void init(HaxeObject listener) {
    	sessionStatusListener = listener;
    }

	public static void login (String permissions) {
		Session session = Session.getActiveSession();
	    if (!session.isOpened() && !session.isClosed())
	    {
	    	session.openForRead(new Session.OpenRequest(mainActivity).setPermissions(Arrays.asList(permissions.split(","))));
	    }
	    else
	    {
	    	Session.openActiveSession(mainActivity, true, Arrays.asList(permissions.split(",")), null);
	    }
	}

	public static void close() {
		Session session = Session.getActiveSession();
		if (session!=null && session.isOpened()) {
			session.closeAndClearTokenInformation();
		}
	}

	public static String getSessionState() {
		if(Session.getActiveSession()!=null)
    		return Session.getActiveSession().getState().name();
    	else
    		return "NULL_SESSION";
	}

	public static void getRequest(final String graphPath, final HaxeObject callbackObject) {
		getRequestWithParams(graphPath, callbackObject, null);		
	}

	/**
	 * Returns a set of the unique names of all query parameters. Iterating
	 * over the set will return the names in order of their first occurrence.
	 *
	 * @throws UnsupportedOperationException if this isn't a hierarchical URI
	 *
	 * @return a set of decoded names
	 */
	private static Set<String> getQueryParameterNames(Uri uri) {
	    if (uri.isOpaque()) {
	        throw new UnsupportedOperationException("This isn't a hierarchical URI.");
	    }

	    String query = uri.getEncodedQuery();
	    if (query == null) {
	        return Collections.emptySet();
	    }

	    Set<String> names = new LinkedHashSet<String>();
	    int start = 0;
	    do {
	        int next = query.indexOf('&', start);
	        int end = (next == -1) ? query.length() : next;

	        int separator = query.indexOf('=', start);
	        if (separator > end || separator == -1) {
	            separator = end;
	        }

	        String name = query.substring(start, separator);
	        names.add(Uri.decode(name));

	        // Move start to end of name.
	        start = end + 1;
	    } while (start < query.length());

	    return Collections.unmodifiableSet(names);
	}

	private static Bundle createParamsBundle(String params) {
		if(params==null) return new Bundle();

		Uri uri = Uri.parse("?"+params);
		Set<String> paramsNames = getQueryParameterNames(uri);

		Bundle parameters = new Bundle(paramsNames.size());

		for (String paramName: paramsNames.toArray(new String[paramsNames.size()])) {
			parameters.putString(paramName, uri.getQueryParameter(paramName));
        }

        return parameters;
	}

	public static void getRequestWithParams(final String graphPath, final HaxeObject callbackObject, String params) {
		final Request request = new Request(Session.getActiveSession(), graphPath, createParamsBundle(params), HttpMethod.GET, new Request.Callback() {
			HaxeObject callback = callbackObject;

			@Override
            public void onCompleted(Response response) {
            	// Process the returned response
                GraphObject graphObject = response.getGraphObject();
    			JSONObject innerJSONObject = null;
                if(graphObject!=null)
                	innerJSONObject = graphObject.getInnerJSONObject();

                FacebookRequestError error = response.getError();
                String errorString = null;

                if(error!=null) {
                	errorString = error.toString();
                }

                String jsonText = null;

                if(innerJSONObject!=null) {
                	jsonText = innerJSONObject.toString();
                	Log.e("trace", "jsonText: "+jsonText);
                }

                callback.call("result",  new Object[]{jsonText, errorString});
			}
        });
		
		mainActivity.runOnUiThread(new Runnable() {
			public void run() {
				// Execute the request asynchronously.
    			Request.executeBatchAsync(request);
    		}
		});
	}
	
	private static Activity getActivity() {
		return mainActivity;
	}

	private static void showDialogWithoutNotificationBar(final String action, final Bundle params) {
		getActivity().runOnUiThread(new Runnable() {
			public void run() {
				WebDialog dialog = new WebDialog.Builder(getActivity(), Session.getActiveSession(), action, params).
			        setOnCompleteListener(new WebDialog.OnCompleteListener() {
			        @Override
			        public void onComplete(Bundle values, FacebookException error) {
			            if (error != null && !(error instanceof FacebookOperationCanceledException)) {
			                /*((HomeActivity)getActivity()).
			                    showError(getResources().getString(R.string.network_error), false);*/
			            }
			        }
			    }).build();

			    Window dialog_window = dialog.getWindow();
			    dialog_window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
			        WindowManager.LayoutParams.FLAG_FULLSCREEN);

			    dialog.show();
			}
		});
	}

	public static void sendRequestDialog(final String message, final String to) {
	    getActivity().runOnUiThread(new Runnable() {
			public void run() {
				Bundle params = new Bundle();
	    		params.putString("message", message);
	    		if(to!=null)
	    			params.putString("to", to);

			    WebDialog requestsDialog = (
			        new WebDialog.RequestsDialogBuilder(getActivity(),
			            Session.getActiveSession(),
			            params))
			            .setOnCompleteListener(new WebDialog.OnCompleteListener() {

			                @Override
			                public void onComplete(Bundle values,
			                    FacebookException error) {
			                    if (error != null) {
			                    	if (error instanceof FacebookServiceException) {
			                            Toast.makeText(getActivity().getApplicationContext(), 
			                                "La solicitud fue cancelada.",
			                                Toast.LENGTH_SHORT).show();
			                                
			                        } else {
			                            Toast.makeText(getActivity().getApplicationContext(), 
			                                "Ha ocurrido un error de conexión.",
			                                Toast.LENGTH_SHORT).show();
			                        }
			                    } else {
			                        final String requestId = values.getString("request");
			                        if (requestId != null) {
			                            Toast.makeText(getActivity().getApplicationContext(), 
			                                "¡Solicitud enviada!",
			                                Toast.LENGTH_SHORT).show();
			                        } else {
			                            Toast.makeText(getActivity().getApplicationContext(), 
			                                "No se pudo enviar la solicitud.",
			                                Toast.LENGTH_SHORT).show();
			                        }
			                    }   
			                }

			            })
	            .build();
	    
				requestsDialog.show();
			}
		});
	    
	}

	public static void share(String link, String name, String caption, String description, String picture) {
	    
	    if (FacebookDialog.canPresentShareDialog(getActivity(), FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {

			// Create the Native Share dialog
	        FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(getActivity())
	        .setLink(link)
	        .setName(name)
	        .setCaption(caption)
	        .setPicture(picture)
	        .build();

	        // Show the Native Share dialog
	        getFbUiLifecycleHelper().trackPendingDialogCall(shareDialog.present());
	    } else {

	        // Prepare the web dialog parameters
	        Bundle params = new Bundle();
	        params.putString("link", link);
	        params.putString("name", name);
	        params.putString("caption", caption);
	        params.putString("description", description);
	        params.putString("picture", picture);

	        // Show FBDialog without a notification bar
	        showDialogWithoutNotificationBar("feed", params);
	    }
	}

	public static String getAccessToken() {
		if(Session.getActiveSession()!=null) {
    		AccessToken accessToken = Session.getActiveSession().getTokenInfo();

    		StringBuilder builder = new StringBuilder();
    		String token = accessToken.getToken();
    		String expires = String.valueOf(accessToken.getExpires().getTime());
    		String lastRefresh = String.valueOf(accessToken.getLastRefresh().getTime());

	        builder.append("{");
	        builder.append("\"token\":\"").append(token==null?"\"\"":token).append("\"");
	        builder.append(", \"permissions\": \"");
	        //builder.append("[");
	        builder.append(TextUtils.join(", ", accessToken.getPermissions())).append("\"");
	        //builder.append("]");
	        builder.append(", \"source\": \"").append(sourceToString(accessToken.getSource())).append("\"");
	        builder.append(", \"expires\": \"").append(expires).append("\"");
	        builder.append(", \"lastRefresh\": \"").append(expires).append("\"");
	        builder.append("}");

	        return builder.toString();
    	}

    	return "";
	}

	public static void setAccessToken(String token, String permissions, String source, String expires, String lastRefresh) {
		Session session = Session.getActiveSession();
	    if (!session.isOpened() && !session.isClosed())
	    {
	    	AccessTokenSource accessTokenSource = convertToSource(source);

	    	Date expiresDate;

	    	try {
	    		expiresDate = new Date(Long.valueOf(expires));
	    	} catch (Exception e) {
	   			expiresDate = null;
	    	}

	    	Date lastRefreshDate;

	    	try {
	    		lastRefreshDate = new Date(Long.valueOf(lastRefresh));
	    	} catch (Exception e) {
	   			lastRefreshDate = null;
	    	}

			AccessToken accessToken = AccessToken.createFromExistingAccessToken(token, expiresDate, lastRefreshDate, accessTokenSource, Arrays.asList(permissions.split(",")));

	    	session.open(accessToken, new Session.StatusCallback() {
		        @Override
		        public void call(Session session, SessionState state, Exception exception) {
		            // Add code here to accommodate session changes
		            statusCallbackCall(session, state);
		        }
    		});
	    }
	}

	private static AccessTokenSource convertToSource(String source) {
		if(source=="0")
			return AccessTokenSource.NONE;

	    if(source=="1")
			return AccessTokenSource.FACEBOOK_APPLICATION_WEB;

	    if(source=="2")
			return AccessTokenSource.FACEBOOK_APPLICATION_NATIVE;

	    if(source=="3")
			return AccessTokenSource.FACEBOOK_APPLICATION_SERVICE;

	    if(source=="4")
			return AccessTokenSource.WEB_VIEW;

	    if(source=="5")
			return AccessTokenSource.TEST_USER;

	    if(source=="6")
			return AccessTokenSource.CLIENT_TOKEN;

		return AccessTokenSource.NONE;
	}

	private static String sourceToString(AccessTokenSource source) {
		if(source==AccessTokenSource.NONE)
			return "0";

	    if(source==AccessTokenSource.FACEBOOK_APPLICATION_WEB)
			return "1";

	    if(source==AccessTokenSource.FACEBOOK_APPLICATION_NATIVE)
			return "2";

	    if(source==AccessTokenSource.FACEBOOK_APPLICATION_SERVICE)
			return "3";

	    if(source==AccessTokenSource.WEB_VIEW)
			return "4";

	    if(source==AccessTokenSource.TEST_USER)
			return "5";

	    if(source==AccessTokenSource.CLIENT_TOKEN)
			return "6";

		return "0";
	}

	/**
	 * Called when an activity you launched exits, giving you the requestCode 
	 * you started it with, the resultCode it returned, and any additional data 
	 * from it.
	 */
	public boolean onActivityResult (int requestCode, int resultCode, Intent data) {
		
		getFbUiLifecycleHelper().onActivityResult(requestCode, resultCode, data);

		return true;
		
	}
	
	
	/**
	 * Called when the activity is starting.
	 */
	public void onCreate (Bundle savedInstanceState) {	
		Fb_haxe_android.fbUiLifecycleHelper = new UiLifecycleHelper(mainActivity, new Session.StatusCallback() {
	        @Override
	        public void call(Session session, SessionState state, Exception exception) {
	            // Add code here to accommodate session changes
	            statusCallbackCall(session, state);
	        }
    	});

    	getFbUiLifecycleHelper().onCreate(savedInstanceState);		
		
	}
	
	
	/**
	 * Perform any final cleanup before an activity is destroyed.
	 */
	public void onDestroy () {
		
		
		
	}
	
	
	/**
	 * Called as part of the activity lifecycle when an activity is going into
	 * the background, but has not (yet) been killed.
	 */
	public void onPause () {
		
		getFbUiLifecycleHelper().onPause();
		
	}
	
	
	/**
	 * Called after {@link #onStop} when the current activity is being 
	 * re-displayed to the user (the user has navigated back to it).
	 */
	public void onRestart () {
		
		
		
	}
	
	
	/**
	 * Called after {@link #onRestart}, or {@link #onPause}, for your activity 
	 * to start interacting with the user.
	 */
	public void onResume () {
		
		getFbUiLifecycleHelper().onResume();
		
	}
	
	
	/**
	 * Called after {@link #onCreate} &mdash; or after {@link #onRestart} when  
	 * the activity had been stopped, but is now again being displayed to the 
	 * user.
	 */
	public void onStart () {
		
		
		
	}
	
	
	/**
	 * Called when the activity is no longer visible to the user, because 
	 * another activity has been resumed and is covering this one. 
	 */
	public void onStop () {
		
		getFbUiLifecycleHelper().onDestroy();
		
	}

	private static void statusCallbackCall(final Session session, final SessionState state) {
		if(sessionStatusListener!=null) {
			mainActivity.runOnUiThread(new Runnable() {
				public void run() {
					String stateName = (state!=null)? state.name():session.getState().name();
					sessionStatusListener.call("sessionStateChanged", new Object[] { stateName });
				}
			});
		} else {
			Log.e("trace","status changed but no callback setted");
		}
	}

	public static String getAppSignature() {
		//Useful code to get the app signature hash
		try {
			String PACKAGE_NAME = mainContext.getPackageName(); //getApplicationContext().getPackageName();
	        PackageInfo info = mainActivity.getPackageManager().getPackageInfo( PACKAGE_NAME, PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				return Base64.encodeToString(md.digest(), Base64.DEFAULT);
			}
	    } catch (NameNotFoundException e) {
		} catch (NoSuchAlgorithmException e) {
		}
		return "";
	}
}