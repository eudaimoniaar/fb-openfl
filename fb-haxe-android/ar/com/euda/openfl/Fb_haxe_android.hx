package ar.com.euda.openfl;

#if cpp
import cpp.Lib;
#elseif neko
import neko.Lib;
#end

#if (android && openfl)
import openfl.utils.JNI;
#end

import flash.events.Event;
import flash.events.EventDispatcher;

class SessionStateChanged extends Event {
	public static var SESSION_STATE_CHANGED:String = "SESSION_STATE_CHANGED";

	public static var CREATED:String = "CREATED";
	public static var CREATED_TOKEN_LOADED:String = "CREATED_TOKEN_LOADED";
	public static var OPENING:String = "OPENING";
	public static var OPENED:String = "OPENED";
	public static var OPENED_TOKEN_UPDATED:String = "OPENED_TOKEN_UPDATED";
	public static var CLOSED_LOGIN_FAILED:String = "CLOSED_LOGIN_FAILED";
	public static var CLOSED:String = "CLOSED";

	public var state:String;

	public function new(state:String) {
		super(SESSION_STATE_CHANGED);

		this.state = state;
	}
}

class RequestCallback {
	private var callback:Dynamic;

	public function new(callback:Dynamic) {
		this.callback = callback;
	}

	public function result(graphObject:String, error:Dynamic): Void {
		var obj:Dynamic = null;
		if(graphObject!=null)
			obj = haxe.Json.parse(graphObject);
		
		haxe.Timer.delay(function() {
			callback(obj, error);
		}, 1);	
	}
}

class Fb_haxe_android extends EventDispatcher {

	public static var instance(get, null):Fb_haxe_android;
	private static var _instance:Fb_haxe_android;

	public static var NO_SESSION:String = "NO_SESSION";

	private static var sessionState:String = NO_SESSION;

	public static function get_instance(): Fb_haxe_android {
		if(_instance!=null) return _instance;

		_instance = new Fb_haxe_android();
		return _instance;
	}

	private function new() {
		super();
	}

	public static function init():Void {
		#if (android && openfl)
		trace("init FB for ios. haxe");
		fb_haxe_android_init(Fb_haxe_android.get_instance());
		#elseif (ios && openfl) 
		fb_haxe_android_init(Fb_haxe_android.get_instance().sessionStateChanged);
		#end
	}

	public function login(permissions:String):Void {
		#if ((android ||ios) && openfl)
		fb_haxe_android_login(permissions);
		#end
	}

	public function getSessionState():String {
		#if ((android ||ios) && openfl)
		return sessionState = fb_haxe_android_get_session_state();
		#else
		return "NO_SESSION";
		#end
	}

		/**
     * Returns a boolean indicating whether the state represents a successfully
     * opened state in which the Session can be used with a {@link Request}.
     * 
     * @return a boolean indicating whether the state represents a successfully
     *         opened state in which the Session can be used with a
     *         {@link Request}.
     */
    public function isOpened(): Bool {
    	getSessionState();
        return (sessionState==SessionStateChanged.OPENED || 
        		sessionState==SessionStateChanged.OPENED_TOKEN_UPDATED);
    }

    /**
     * Returns a boolean indicating whether the state represents a closed
     * Session that can no longer be used with a {@link Request}.
     * 
     * @return a boolean indicating whether the state represents a closed
     * Session that can no longer be used with a {@link Request}.
     */
    public function isClosed(): Bool {
    	getSessionState();
        return (sessionState==SessionStateChanged.CLOSED_LOGIN_FAILED || 
        		sessionState==SessionStateChanged.CLOSED);
    }

	public function sessionStateChanged(sessionState:String): Void {
		dispatchEvent(new SessionStateChanged(sessionState));
	}

	public function getRequest(graphPath:String, callback:Dynamic, params:String=null): Void {
		#if (android && openfl)
		if(params==null)
			fb_haxe_android_get_request(graphPath, new RequestCallback(callback));
		else 
			fb_haxe_android_get_request_with_params(graphPath, new RequestCallback(callback), params);
		#elseif (ios && openfl)
			fb_haxe_android_get_request(graphPath, params, new RequestCallback(callback).result);
		#end
	}

	public function sendRequestDialog(message:String, to:String):Void {
		#if ((android || ios) && openfl)
		fb_haxe_android_send_request_dialog(message, to);
		#end
	}

	public function share(link:String, name:String, caption:String, description:String, picture:String):Void {
		#if (android && openfl)
		fb_haxe_android_share(link,name,caption,description,picture);
		#elseif (ios && openfl)
		fb_haxe_android_share(link);
		#end
	}

	public function close(): Void {
		#if ((android || ios) && openfl)
		fb_haxe_android_close();
		#end
	}

	public static function getAppSignature():String {
		#if (android && openfl)
		return fb_haxe_android_get_app_signature();
		#else 
		return "";
		#end
	}

	public function getAccessToken():String {
		#if ((android || ios) && openfl)
		var token = fb_haxe_android_get_access_token();
		trace("token: "+token);
		return token;
		#else 
		return "";
		#end
	}

	/**
	 * First init!
	 */
	public function setAccessToken(jsonToken:String):Void {
		#if ((android || ios) && openfl)
		var token:Dynamic = haxe.Json.parse(jsonToken);

		var accessToken:String = token.token;
		var permissions:String = token.permissions;
		var expires:String = token.expires;
		var lastRefresh:String = token.lastRefresh;
		#end

		trace("token: "+accessToken);
		trace("permissions: "+permissions);
		trace("expires: "+expires);
		trace("lastRefresh: "+lastRefresh);

		#if (android && openfl)
		var source:String = token.source;
		trace("source: "+source);
		fb_haxe_android_set_access_token(accessToken, permissions, source, expires, lastRefresh);
		#end

		#if (ios && openfl)
		var appID:String = token.appID;
		var userID:String = token.userID;
		trace("appID: "+appID);
		trace("userID: "+userID);
		fb_haxe_android_set_access_token(accessToken, permissions, appID, userID, expires/*, lastRefresh*/);
		#end		
	}

	#if (android && openfl)
	private static var fb_haxe_android_init = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "init", "(Lorg/haxe/lime/HaxeObject;)V");
	private static var fb_haxe_android_get_session_state = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "getSessionState", "()Ljava/lang/String;");
	private static var fb_haxe_android_login = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "login", "(Ljava/lang/String;)V");
	private static var fb_haxe_android_get_app_signature = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "getAppSignature", "()Ljava/lang/String;");
	private static var fb_haxe_android_get_request = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "getRequest", "(Ljava/lang/String;Lorg/haxe/lime/HaxeObject;)V");
	private static var fb_haxe_android_get_request_with_params = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "getRequestWithParams", "(Ljava/lang/String;Lorg/haxe/lime/HaxeObject;Ljava/lang/String;)V");
	private static var fb_haxe_android_close = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "close", "()V");
	private static var fb_haxe_android_send_request_dialog = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "sendRequestDialog", "(Ljava/lang/String;Ljava/lang/String;)V");
	private static var fb_haxe_android_share = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "share", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
	private static var fb_haxe_android_get_access_token = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "getAccessToken", "()Ljava/lang/String;");
	private static var fb_haxe_android_set_access_token = JNI.createStaticMethod ("org.haxe.extension.Fb_haxe_android", "setAccessToken", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
	#elseif (ios && openfl)
	private static var fb_haxe_android_init = Lib.load ("fb_haxe_android", "fb_haxe_android_init", 1);
	private static var fb_haxe_android_get_session_state = Lib.load ("fb_haxe_android", "fb_haxe_android_get_session_state", 0);
	private static var fb_haxe_android_login = Lib.load ("fb_haxe_android", "fb_haxe_android_login", 1);
	private static var fb_haxe_android_get_request = Lib.load ("fb_haxe_android", "fb_haxe_android_get_request", 3);
	private static var fb_haxe_android_share = Lib.load ("fb_haxe_android", "fb_haxe_android_share", 1);
	private static var fb_haxe_android_set_access_token = Lib.load ("fb_haxe_android", "fb_haxe_android_set_access_token", 5);
	private static var fb_haxe_android_get_access_token = Lib.load ("fb_haxe_android", "fb_haxe_android_get_access_token", 0);
	private static var fb_haxe_android_send_request_dialog = Lib.load ("fb_haxe_android", "fb_haxe_android_send_request_dialog", 2);
	private static var fb_haxe_android_close = Lib.load ("fb_haxe_android", "fb_haxe_android_close", 0);
	#end
}
