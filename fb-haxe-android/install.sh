#!/bin/bash

echo 'Register fb-haxe-android with haxelib'
haxelib dev fb-haxe-android `pwd`

if [ $? -eq 0 ]; then
  echo '[OK]'
else
  echo '[ERROR]'
fi

