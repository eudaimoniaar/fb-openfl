package;

import openfl.Assets;

import ar.com.euda.openfl.hamburga.Game;
import ar.com.euda.openfl.hamburga.ScreenManager;
import ar.com.euda.openfl.hamburga.AssetsManager;
import ar.com.euda.openfl.hamburga.ScaledSprite;
import ar.com.euda.openfl.hamburga.UIScreen; 
import ar.com.euda.openfl.hamburga.Version;

import ar.com.euda.openfl.Fb_haxe_android;

class Main extends Game {
	
	var defaultFont = Assets.getFont("assets/argentina.ttf");

	public function new () {
		
		super(720, 1280, defaultFont, new Version(0,1));

		trace("App signature: "+Fb_haxe_android.getAppSignature());
		Fb_haxe_android.init();

		screenManager.pushScreen(new FBTestScreen(), false);
	}
	
}