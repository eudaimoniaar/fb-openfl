package;

import flash.display.Loader;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLLoaderDataFormat;
import flash.display.Bitmap;
import flash.events.Event;
import flash.utils.ByteArray;

import ar.com.euda.openfl.hamburga.UIScreen;

class TestLoadScreen extends UIScreen {
	
	private var urlLoader:URLLoader = new URLLoader();

	public function new () {
		super(0x000000);


		/*
		var image_loader = new Loader();
        image_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(_) {
            var bmp:Bitmap = cast image_loader.content;
            trace("Loaded image " + bmp.bitmapData.width + "x" + bmp.bitmapData.height);
        });

        var request:URLRequest = new URLRequest("http://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/c66.110.828.739/s200x200/561068_467819543252887_1507386963_n.jpg");
        request.data = "oh=2100a082702479071dc018775cb37257&oe=54B46A63&__gda__=1422277870_4c1a56944d03d9c0bd713aaf7c89093e";
        image_loader.load(request);
        image_loader.x = 180;
        image_loader.y = 180;
        addChild(image_loader);
		*/


        urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
        //urlLoader.load(new URLRequest("http://upload.wikimedia.org/wikipedia/en/7/72/Example-serious.jpg"));
        urlLoader.load(new URLRequest("http://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/c66.110.828.739/s200x200/561068_467819543252887_1507386963_n.jpg?oh=2100a082702479071dc018775cb37257&oe=54B46A63&__gda__=1422277870_4c1a56944d03d9c0bd713aaf7c89093e"));

        urlLoader.addEventListener(Event.COMPLETE, completeHandler);
    }

    private function completeHandler(event:Event):Void {
        var loader:Loader = new Loader();
        loader.loadBytes(urlLoader.data);
        addChild(loader);
    }
}
