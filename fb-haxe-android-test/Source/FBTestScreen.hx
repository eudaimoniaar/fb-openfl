package;

import flash.events.MouseEvent;
import flash.events.Event;
import flash.display.Sprite;

import ar.com.euda.openfl.hamburga.UIScreen;
import ar.com.euda.openfl.hamburga.AssetsManager;
import ar.com.euda.openfl.hamburga.ScaledSprite;
import ar.com.euda.openfl.hamburga.WebSprite;
import ar.com.euda.openfl.hamburga.ImageButton;
import ar.com.euda.openfl.hamburga.DynamicLabel;
import ar.com.euda.openfl.hamburga.TextUtil;
import ar.com.euda.openfl.hamburga.ScrollableSprite;
import ar.com.euda.openfl.hamburga.PlainButton;

import ar.com.euda.openfl.Fb_haxe_android;

class FBTestScreen extends UIScreen {

	var picture:WebSprite;
	var fb_name:DynamicLabel;
	var uid:DynamicLabel;
	var noImage:ScaledSprite;
	var friendsScroll:ScrollableSprite;

	public function new() {
		super(0xFFFFFF, 1);

		var btnInvite:ImageButton = new ImageButton(this, "assets/button.png", "Reload Token", 0x000000);
		addChild(btnInvite);
		btnInvite.scaleMode = MAINTAIN_ASPECT;
		btnInvite.setPositionMode(CENTER_X, ALIGN_TOP);
		btnInvite.addEventListener(MouseEvent.CLICK, function(e:MouseEvent) {
			//Fb_haxe_android.instance.sendRequestDialog("Mensaje de la invitación");
			trace("token: "+Fb_haxe_android.instance.getAccessToken());
			Fb_haxe_android.instance.setAccessToken(Fb_haxe_android.instance.getAccessToken());
		});

		var btnMe:ImageButton = new ImageButton(this, "assets/button.png", "Me", 0x000000);
		addChild(btnMe);
		btnMe.scaleMode = MAINTAIN_ASPECT;
		btnMe.setPositionMode(CENTER_X, ALIGN_TOP);
		btnMe.setMargin(0,0,100,0);

		Fb_haxe_android.instance.addEventListener(SessionStateChanged.SESSION_STATE_CHANGED, onSessionStateChanged);

		noImage = AssetsManager.instance.getSprite("assets/no-image.png");

		picture = new WebSprite(null, noImage);
		addChild(picture);
		picture.scaleMode = MAINTAIN_ASPECT;
		picture.setPositionMode(CENTER_X, CENTER_Y);
		picture.setMargin(0,0,0,picture.height, false);

		fb_name = TextUtil.createDynamicLabel("Nombre", 46, 0x000000, false);
		addChild(fb_name);
		fb_name.scaleMode = MAINTAIN_ASPECT;
		fb_name.setPositionMode(CENTER_X, CENTER_Y);

		uid = TextUtil.createDynamicLabel("id", 46, 0x000000, false);
		addChild(uid);
		uid.scaleMode = MAINTAIN_ASPECT;
		uid.setPositionMode(CENTER_X, RELATIVE_Y);
		uid.y = fb_name.y + fb_name.height;

		btnMe.addEventListener(MouseEvent.CLICK, function(e:Event) {
			if(Fb_haxe_android.instance.isOpened()){
				testGetRequest();
			} else {
				trace("need login");
				Fb_haxe_android.instance.login("public_profile, user_friends");
			}
		});

		friendsScroll = new ScrollableSprite(width, 200, false, true);
		addChild(friendsScroll);
		friendsScroll.scaleMode = MAINTAIN_ASPECT;
		friendsScroll.setPositionMode(CENTER_X, RELATIVE_Y);
		friendsScroll.y = uid.y + uid.height + height*0.1;
	}

	private function onSessionStateChanged(e:SessionStateChanged): Void {
		trace("onSessionStateChanged: "+e.state);
		if(Fb_haxe_android.instance.isOpened()) {
			testGetRequest();
		}
	}

	private function testGetRequest(): Void {
		Fb_haxe_android.instance.getRequest("/me", onGetRequestDone);

		Fb_haxe_android.instance.getRequest("/me/picture", onUserPicture, "redirect=false&height="+Math.round(noImage.height)+"&width="+Math.round(noImage.width));

	//	Fb_haxe_android.instance.getRequest("/me/friends", onPlayingFriendsLoaded);
		Fb_haxe_android.instance.getRequest("/me/invitable_friends", onInvitableFriendsLoaded);
	}

	public function onPlayingFriendsLoaded(friends:Dynamic, error:Dynamic): Void {
		if(error!=null || friends==null) { 
			trace("Error: "+error);
			return;
		}

		trace("Playing friends: "+friends);
	}

	public function onInvitableFriendsLoaded(friends:Dynamic, error:Dynamic): Void {
		if(error!=null || friends==null) { 
			trace("Error: "+error);
			return;
		}

		trace("Invitable friends: "+friends);

		var friendsContainer:ScaledSprite = new ScaledSprite();
		var i = 0;
		var friendsArray:Array<Dynamic> = cast(friends.data, Array<Dynamic>);
		for(friend in friendsArray) {
			if(i<10) {
			var httpUrl:String = StringTools.replace(friend.picture.data.url,"https", "http");
			var friendSprite = new FBFriend(friend.name, httpUrl, i);
			friendsContainer.addChild(friendSprite);

			friendSprite.x = 15 + (friendSprite.width + 15) * i;
			}
			trace("friend: "+i);
			i++;
		}
		//addChild(friendsContainer);
		//friendsContainer.setPositionMode(CENTER_X, ALIGN_BOTTOM);

		friendsScroll.set_content(friendsContainer);
	}

	public function onGetRequestDone(graphObject:Dynamic, error:Dynamic): Void {
		if(error!=null || graphObject==null) { 
			trace("Error: "+error);
			return;
		}

		fb_name.text = graphObject.name;
		uid.text = graphObject.id;
	}

	public function onUserPicture(pictureInfo:Dynamic, error:Dynamic): Void {
		if(error!=null || pictureInfo==null) {
			trace("onUserPicture error: "+error.toString());
			return;
		}

		if(!pictureInfo.data.is_silhouette) {
			var httpUrl:String = StringTools.replace(pictureInfo.data.url,"https", "http");
			picture.url = httpUrl;
		}
	}
}