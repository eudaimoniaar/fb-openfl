package;

import ar.com.euda.openfl.hamburga.ScaledSprite;
import ar.com.euda.openfl.hamburga.TextUtil;
import ar.com.euda.openfl.hamburga.Label;
import ar.com.euda.openfl.hamburga.WebSprite;

class FBFriend extends ScaledSprite {
	
	public function new(name:String, pictureUrl:String, i:UInt) {
		super();

		graphics.beginFill(0x000000, 0.4);
		graphics.lineStyle(1.0, 0x000000, 1);
		graphics.drawRect(0, 0, 150, 200);
		graphics.endFill();

		var lblName:Label = TextUtil.createLabel(name, 24, 0xFFFFFF, false);
		if(lblName.width>60)
			lblName.scaleX = lblName.scaleY = 150 / lblName.width * 0.9;
		addChild(lblName);
		lblName.setPositionMode(CENTER_X, ALIGN_BOTTOM);


		var picture:WebSprite = new WebSprite(pictureUrl);
		picture.scaleX = picture.scaleY = 3 * 0.9;
		addChild(picture);
		picture.setPositionMode(RELATIVE_X, RELATIVE_Y);
		picture.x = 8;
		picture.y = 10;
	}
}