package;


import flash.display.Sprite;    
import flash.display.Bitmap;
import flash.display.BitmapData;
// This is what we need to retrieve the image
import flash.display.Loader;    
// This is how we specify the location of the image
import flash.net.URLRequest;    

// Downloading an image can dispatch a lot of different event types!
import flash.events.Event;              
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;

import ar.com.euda.openfl.hamburga.UIScreen;

// For timing
import flash.Lib; 

import openfl.net.URLLoader;
import openfl.net.URLLoaderDataFormat;

class LoadingScreen extends UIScreen {
        
        
        private var request:URLRequest;
        private var loader:Loader;
        private var status:Int; // This will store the last HTTP status
        
        private var lastTime:Float;
        private var loading:Bool;
        
        //private var image:Sprite;

        public function new () {
                super(0x000000);
                
                //image = new Sprite();
                //addChild(image);


                // Instantiate the guys!
                //request = new URLRequest( 'http://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/v/t1.0-1/c66.110.828.739/s200x200/561068_467819543252887_1507386963_n.jpg?oh=2100a082702479071dc018775cb37257&oe=54B46A63&__gda__=1422277870_4c1a56944d03d9c0bd713aaf7c89093e' );
                //request = new URLRequest( 'http://vancam.ca/vancam.jpg' );
                request = new URLRequest( 'http://www.eudaimonia.com.ar/w3/img/stores/bt_bb.png' );
                
                loader = new Loader();
                //loader.dataFormat = URLLoaderDataFormat.BINARY;
                // Prepare for loader.load()'s billion events
                loader.addEventListener( Event.COMPLETE, onComplete );
                loader.addEventListener( Event.OPEN, onOpen );
                loader.addEventListener( ProgressEvent.PROGRESS, onProgress );
                loader.addEventListener( SecurityErrorEvent.SECURITY_ERROR, onSecurity );
                loader.addEventListener( HTTPStatusEvent.HTTP_STATUS, onHTTPStatus );
                loader.addEventListener( IOErrorEvent.IO_ERROR, onIOError );
                loader.load( request );
                
                // Don't want to load more than one at a time that would be ridiculous
                loading = true;
                
                // Prepare for timing
                //lastTime = Lib.getTimer();
                //addEventListener( Event.ENTER_FRAME, onFrame );
                
        }
        
        
        private function onFrame( e:Event ) {
                var time = Lib.getTimer();
                var delta = time - lastTime;
                if ( delta > 1000 * 60 ) { // Should be about 1 minute, if my guesses were correct
                        if ( !loading ) { // If it's already loading we shouldn't initiate a new load
                                loader.load( request );
                                lastTime = time;
                        }
                }
        }
        
        
         private function onComplete(event:Event):Void {
                trace("onComplete");
                if ( status == 200 ) {  // 200 is a successful HTTP status
                        trace("http 200");
                        var b:Bitmap = event.target.content;    // This is where the bitmap lives
                
                        // I cut out some scaling and positioning logic here
                        // ...
                        
                        // Remove the old children (older versions of the bitmap).. You know, for memory reasons
                        //while( image.numChildren > 0 ) {
                        //        var a:Bitmap = cast( image.removeChildAt( 0 ), Bitmap );
                        //        //if(a!=null && a.bitmapData!=null)
                        //        //    a.bitmapData.dispose();                         
                        //}
                      
                        // Add the new one
                        addChild( b );
                        
                } 
                
                // Loading process is now complete
                loading = false;
        }
        
        private function onOpen(event:Event):Void {
                // The download has begun!
                loading = true;
                trace("onOpen");
        }

        private function onProgress(event:ProgressEvent):Void {
                var p = event.bytesLoaded/event.bytesTotal;
                // Math.round(p*100) is the current percent of bytes retrieved
                trace("Download progress "+Math.round(p*100)+"%");
        }

        private function onSecurity(event:SecurityErrorEvent):Void {
                trace("securityErrorHandler: " + event);
                loading = false;
        }

        private function onHTTPStatus(event:HTTPStatusEvent):Void {
                // Hopefully this is 200
                status = event.status;
                trace("onHTTPStatus: "+status);
        }

        private function onIOError(event:IOErrorEvent):Void {
                trace("ioErrorHandler: " + event);
                loading = false;
        }

}